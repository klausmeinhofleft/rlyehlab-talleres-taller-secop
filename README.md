# Taller de SecOp

Este repo contiene la informacion y presentaciones para un curso introductorio de 3 días para Periodistas y movimientos sociales que quieran acercarse al lab para aprender sobre conocimientos básicos de seguridad y prácticas de cuidado.


## Contenido

### El siguiente repositorio debería tener:

* Almenos una presentación en reveal.js
* Documentación sobre los temas dados
* Guía para la persona exponente
* Bibliografía

## Dinámica

### El taller se dará en 3 días con almenos 1 semana entre ellos. Cada día abordará un tema diferente.

* Dia 1: Conceptos sobre seguridad y buenas prácticas
* Día 2: Uso e implementación de herramientas
* Día 3: Puesta en valor y planificación

