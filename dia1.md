## Dia 1

Se habló de la organización, sus preocupaciones, el riesgo al que están expuestos, la infra y formas de comunicación. 
Se discutió acerca de diferentes herramientas como tor, signal, keepassx, etc. Así como de metadata privacidad, anonimato, threadmodel.

## Se discutió sobre:

* TOR
 - `https://check.torproject.org/`
 - Whonix
 
* Fingerprint
 - `https://amiunique.org/`

* TAILS
  - Instalación y booteo
  - EtcherIO

* Cookies / traking
  - Privacy badger
  - NoSript
  - Https everywhere
  - mublock
  - Decentral eyes
  - Disconnect

* GPG
  - Instalación de Thunderbird y Enigmail en Windows
  - Subir llaves
  - Firma de llaves
  - Openkeychain


## Objetivos del curso

* Crear protocolo de seguridad y contingencia
  - Reforzar el uso de herramientas seguras
  - Ciclo de vida de contraseñas y confidencialidad
  - Entender el alcance del riesgo
  - Protocolo de recuperación de desastres
    - Canary
    - Avisar a compañeros
    - Cambiar de identidades y fingerprints
  - Recupero de la información por backup

* Aprender a asegurar dispositivos personales y utilizar herramientas adecuadas
  - Borrar metadata
  - Cifrar dispositivos
  - Signal
  - GPG
  - 2FA
  - Password Manager / keepass - keepass2android
  - Tor
  - Navegación segura / VPN

* Proyectar mejoras en la infraestructura
  - Backups
  - UPS, estabilizadores, electricidad
  - Wifi
  - Emails por SSL
  - Seguridad del Wordpress
  - Seguridad del Google Drive
  - Servicio de VPN
  - Servidores on promise
  - Implementar Whonix

* Crear manual para capacitación interna
  - Organizativo / informativo
  - Técnico / práctico
