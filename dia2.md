## Día 2

Se discutió sobre la utilización de las herramientas mencionadas el dia 1 entre ellas:

- Passwords:
Instalación de Keepassx, utilización y optimización en la administración de las contraseñas. Tanto en pc como en el celular.

- FreeOTP:
Si no hay otra alternativa a OTP no utilizarlo. Guardarlo en el keepass.
Securización de cuentas.
    - Como tip, tomar captura del QR y tenerlo guardado.

- TorBrowser:
Instalación y utilización

- CheckSum
Checksum de hashes sha256 y gpg

Linux
    - sha256
    - gpg --verify

Windows
    - gpg4win

- VPN's
No se llegó a un acuerdo, sino que queda de parte de la ONG decidir si la quieren implementar o no, lo que si es que es mejor optar por un servicio pago y no gratuito.

- Metadata
Borrado de metadata de fotos y archivos.
Linux
    - MAT Documentos
    - pdf-redact-tools Pdf's
    - exiftools Fotos y Videos(?)
    
Windows
    - Click derecho propiedades
    - exiftools
